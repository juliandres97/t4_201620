package taller.test;

import java.util.Random;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.HybridSortTeam;

public class HybridSortTest extends TestCase{
	
	Comparable[] arr;
	int lo;
	int hi;
	Random random = new Random();
	
	private void setupEscenario1()
	{
		lo = -50;
		hi = 50;
		arr = new Comparable[hi-lo];
		for(int i = 0; i< hi-lo; i++)
		{
			arr[i] = randInt(lo, hi);
		}
		HybridSortTeam team = new HybridSortTeam();
		team.sort(arr, TipoOrdenamiento.ASCENDENTE);
	}
	private void setupEscenario2()
	{
		lo = -50;
		hi = 50;
		arr = new Comparable[hi-lo];
		for(int i = 0; i< hi-lo; i++)
		{
			arr[i] = randInt(lo, hi);
		}
		HybridSortTeam team = new HybridSortTeam();
		team.sort(arr, TipoOrdenamiento.DESCENDENTE);
	}
	private void setupEscenario3()
	{
		lo = -2555;
		hi = 2500;
		arr = new Comparable[hi-lo];
		for(int i = 0; i< hi-lo; i++)
		{
			arr[i] = randInt(lo, hi);
		}
		HybridSortTeam team = new HybridSortTeam();
		team.sort(arr, TipoOrdenamiento.ASCENDENTE);
	}

    private int randInt(int min, int max) 
    {
        int randomNum = random.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    
    public boolean ordenado(Comparable a, Comparable b, TipoOrdenamiento orden)
    {
    	if(orden.equals(TipoOrdenamiento.ASCENDENTE) && 
				a.compareTo(b) > 0)
		{
			return false;
		}
		else if(orden.equals(TipoOrdenamiento.DESCENDENTE) && 
				a.compareTo(b) < 0 )
		{
			return false;
		}
		return true;
    }

    
    private boolean checkOrden(TipoOrdenamiento orden)
    {
    	boolean bien = true;
    	
    	for(int i = 0; bien && i<arr.length-1; i++)
    	{
    		bien = ordenado(arr[i], arr[i+1], orden);
    	}
    	return bien;
    	
    	
    }
    
    public void testBienOrdenado1()
    {
    	setupEscenario1();
    	
    	assertEquals("La lista no contiene todos los elementos iniciales." , hi-lo, arr.length);
    	
    	for(int i = 0; i<arr.length; i++)
    	{
    		System.out.println(arr[i]);
    	}
    	
    	assertTrue("La lista no se encuentra ordenada correctamente.", checkOrden(TipoOrdenamiento.ASCENDENTE));	
    }
    
    public void testBienOrdenado2()
    {	
    	setupEscenario2();
    	
    	assertEquals("La lista no contiene todos los elementos iniciales." , hi-lo, arr.length);
    	assertTrue("La lista no se encuentra ordenada correctamente.", checkOrden(TipoOrdenamiento.DESCENDENTE));	
    }
    
    public void testBienOrdenado3()
    {		
    	setupEscenario3();
    	
    	assertEquals("La lista no contiene todos los elementos iniciales." , hi-lo, arr.length);
    	assertTrue("La lista no se encuentra ordenada correctamente.", checkOrden(TipoOrdenamiento.ASCENDENTE));
    }	
}