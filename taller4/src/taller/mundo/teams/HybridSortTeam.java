package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class HybridSortTeam extends AlgorithmTeam {

	public HybridSortTeam() {
		super("HybridSort (*)");
		// TODO Auto-generated constructor stub
	}

	@Override
	public Comparable[] sort(Comparable[] arr, TipoOrdenamiento orden) {
		// TODO Auto-generated method stub
		if (arr.length < 1000)
		{
			for (int i = 1; i < arr.length; i++) 
			{
				Comparable aInsertar  = arr[i];
				int j = i;
				
				while ((j > 0) && (getState(arr[j-1], aInsertar, orden))) 
				{
					arr[j] = arr[j-1];
					j--;
				}
				arr[j] = aInsertar;
			}
		}
		else
		{
			for (int i = 0; i < arr.length - 1; i++)
			{
				int minimo = i;
				int maximo = i;
				for (int j = i+1; j < arr.length; j++)
				{
					if (orden == TipoOrdenamiento.ASCENDENTE)
					{
						if (arr[j].compareTo(arr[minimo]) == -1)
							minimo = j;					
					}
					else if (orden == TipoOrdenamiento.DESCENDENTE)
					{
						if (arr[j].compareTo(arr[maximo]) == 1)
							maximo = j;
					}
				}
				if (minimo != i)
				{
					Comparable temp = arr[i];
					arr[i] = arr[minimo];
					arr[minimo] = temp;
				}
				
				if (maximo != i)
				{
					Comparable temp = arr[maximo];
					arr[maximo] = arr[i];
					arr[i] = temp;
				}
			}
		}
		
		return arr;
	}
	
	private boolean getState(Comparable a, Comparable b, TipoOrdenamiento orden)
	{
		int stat = a.compareTo(b);
		boolean val = stat > 0 && orden == TipoOrdenamiento.ASCENDENTE;
		val = val || stat < 0 && orden == TipoOrdenamiento.DESCENDENTE;
		return val;
	}
}