package taller.mundo.teams;

/*
 * QuickSortTeam.java
 * Tfins file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import java.util.Random;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class QuickSortTeam extends AlgorithmTeam
{

	private static Random random = new Random();

	public QuickSortTeam()
	{
		super("Quicksort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		quicksort(list, 0, list.length, orden);
		return list;
	}
	// Trabajo en Clase

//	Tomado de:
//    http://www.algolist.net/Algorithms/Sorting/Quicksort
	private static void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		int j = particion(lista, inicio, fin, orden);

		if (inicio < (j - 1))
			quicksort(lista, inicio, (j - 1), orden);

		if (j < fin)
			quicksort(lista, j, fin, orden);
	}

	private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		//Nunca deber�a retornarse un valor negativo
		int retorno = -1;	
		int i = inicio;
		int j = fin + 1;
		int indicePivote = eleccionPivote(inicio, fin);
		Comparable pivote = lista[indicePivote];

		while (true) { 
			// find item on lo to swap
			while (lista[++i].compareTo(pivote) < 0)
				if (i == fin) break;

			// find item on fin to swap
			while (pivote.compareTo(lista[--j]) < 0)
				if (j == inicio) break;      // redundant since a[inicio] acts as sentinel

			// check if pointers cross
			if (i >= j) break;

			Comparable temp = lista[i];
			lista[i] = lista[j];
			lista[j] = temp;
		}

		// put partitioning item pivote at a[j]
		Comparable temp = pivote;
		pivote = lista[j];
		lista[j] = temp;

		// now, a[inicio .. j-1] <= a[j] <= a[j+1 .. fin]
		retorno = j;

		return retorno;
	}

	private static int eleccionPivote(int inicio, int fin)
	{
		/**
           Este procedimiento realiza la elecci�n de un �ndice que corresponde al pivote res-
           pecto al cual se realizar�  la partici�n de la lista. Se recomienda escoger el ele-
           mento que se encuentra en la mitad, o de forma aleatoria entre los �ndices [inicio, fin).
		 **/
		// Trabajo en Clase
		double pivoteMitadORandom = Math.random()*100;
		int pivote = randInt(inicio, fin);

		if (pivoteMitadORandom > 50) {
			pivote = (inicio + fin)/2;
		}
		return pivote;
	}

	/**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
	 **/
	public static int randInt(int min, int max) 
	{
		int randomNum = random.nextInt((max - min) + 1) + min;
		return randomNum;
	}
	// Trabajo en Clase
}
