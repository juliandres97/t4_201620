package taller.mundo.teams;

/*
 * SelectionSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class SelectionSortTeam extends AlgorithmTeam
{
	public SelectionSortTeam()
	{
		super("Selection Sort (-)");
		userDefined = false;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		return selectionSort(list, orden);
	}

	/**
      Ordena un arreglo de enteros, usando Ordenamiento por selección.
      @param arr Arreglo de enteros.
	 **/
//	Tomado de:
//	http://www.algolist.net/Algorithms/Sorting/Selection_sort
//	http://mathbits.com/MathBits/Java/arrays/SelectionSort.htm
	private Comparable[] selectionSort(Comparable[] arr, TipoOrdenamiento orden)
	{
		// Para hacer en casa
		
		for (int i = 0; i < arr.length - 1; i++)
		{
			int minimo = i;
			int maximo = i;
			for (int j = i+1; j < arr.length; j++)
			{
				if (orden == TipoOrdenamiento.ASCENDENTE)
				{
					if (arr[j].compareTo(arr[minimo]) == -1)
						minimo = j;					
				}
				else if (orden == TipoOrdenamiento.DESCENDENTE)
				{
					if (arr[j].compareTo(arr[maximo]) == 1)
						maximo = j;
				}
			}
			if (minimo != i)
			{
				Comparable temp = arr[i];
				arr[i] = arr[minimo];
				arr[minimo] = temp;
			}
			
			if (maximo != i)
			{
				Comparable temp = arr[maximo];
				arr[maximo] = arr[i];
				arr[i] = temp;
			}
		}
		return arr;
	}
}