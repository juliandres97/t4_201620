package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
	public MergeSortTeam()
	{
		super("Merge sort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		return merge_sort(lista, orden);
	}

//	Tomado de:
//	http://stackoverflow.com/questions/13727030/mergesort-in-java
//	http://www.vogella.com/tutorials/JavaAlgorithmsMergesort/article.html
	private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		if(lista.length <= 1) {
			return lista;
		}

		int mid = lista.length/2;
		Comparable[] izquierda = Arrays.copyOfRange(lista, 0, mid);
		Comparable[] derecha = Arrays.copyOfRange(lista, mid, lista.length);

		izquierda = merge_sort(izquierda, orden);
		derecha = merge_sort(derecha, orden);

		lista = merge(izquierda,derecha, orden);

		return lista;
	}

	private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
	{
		int totalElementos = izquierda.length + derecha.length;
		Comparable[] respuesta = new Comparable[totalElementos];
		int i = 0;
		int j = 0;
		for(int k = 0; k < respuesta.length; k++)
		{
			if(i == izquierda.length)
				respuesta[k] = derecha[j++];

			else if(j == derecha.length)
				respuesta[k] = izquierda[i++];

			else if(!getState(izquierda[i], derecha[j], orden))
				respuesta[k] = izquierda[i++];

			else
				respuesta[k] = derecha[j++];
		}

		return respuesta;
	}

	private static boolean getState(Comparable a, Comparable b, TipoOrdenamiento orden)
	{
		int stat = a.compareTo(b);
		boolean val = stat > 0 && orden == TipoOrdenamiento.ASCENDENTE;
		val = val || stat < 0 && orden == TipoOrdenamiento.DESCENDENTE;
		return val;
	}
}